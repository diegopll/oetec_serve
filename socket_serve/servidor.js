//CREACION DE SOCKET SERVIDOR CON NODEJS Y SOCKETIO

var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var Client = require('node-rest-client').Client;
var bodyParser = require("body-parser");
var request = require('request');

var client = new Client();
var router = express.Router();

app.use(express.static('public'));
const ip = "192.168.1.8:8050";

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
/**
 * SOCKET CREADO PARA CONCECION
 * ############################################################################
 */
io.on('connection', function (socket) {

  /** MENSAJES DE CONEXION EXITOSA */
  var socketId = socket.id;
  var clientIp = socket.request.connection.remoteAddress;
  console.log('CLIENTE se ha conectado con Sockets');
  console.log(clientIp + "  " + socketId);

  /** EVENTO QUE USUARIO CLIENTE PARA NOTIFICACION*/
  /**
   * SOCKET CREADO PARA SOLICITAR SER VOLUNTARIO DE UNA ORGANIZACION
   * EL VOLUNTARIO SOLICITARA SER VOLUNTARIO
   * Y SE NOTIFICARA AL MOVIL ORGANIZACION Y TENDRA QUE VALIDAR QUE EL ID ENVIADO PARA RECIBIR SU NOTIFICACION
   */
  socket.on('solicitar-ser-voluntario', function (pk_org, pk_volun) {
    //AQUI SE RECIBE LOS DATOS DESDE EL CLIENTE    
    var args = {
      data: {
        organizacion: pk_org,
        voluntario: pk_volun
      },
      headers: { "Content-Type": "application/json" }
    };
    var req = client.post("http://" + ip + "/api-usuario/solicitud-voluntario/", args, function (data, response) {
      if (response.statusCode == '201') {
        var message_notificacion = {
          mensaje: "Un usuario ha solicitado ser voluntario",
          pk: pk_org
        };
        // SOCKET PARA NOTIFICAR ORGANIZACION      
        io.sockets.emit('notificacion', message_notificacion);

        var message_notificacion_volun = {
          mensaje: data.response,
          pk: pk_volun
        };        
        // SOCKET PARA ENVIAR VALIDACION A VOLUNTARIO
        io.sockets.emit('error', message_notificacion_volun);
      } else {                
        var message_notificacion_volun = {
          mensaje: data.error,
          pk: pk_volun
        };        
        // SOCKET PARA REENVIAR ERRORES A VOLUNTARIO
        io.sockets.emit('error', message_notificacion_volun);
      }
    });
    req.on('error', function (err) {
      io.sockets.emit('error', err);
    });
  });

  /**
   * SOCKET CREADO PARA APROBAR VOLUNTARIO POR ORGANIZACION
   * LA ORGANIZACION APROBARA 
   * Y SE NOTIFICARA AL MOVIL VOLUNTARIO Y TENDRA QUE VALIDAR QUE EL ID ENVIADO PARA RECIBIR SU NOTIFICACION
   */
  socket.on('aceptar-voluntario', function (pk_org, pk_volun) {
    //AQUI SE RECIBE LOS DATOS DESDE EL CLIENTE        
    //http://localhost:8050/api-usuario/voluntario-organizacion/pk-organizacion/?pk_voluntario=03e9470e-f6c4-447e-89aa-4f39b6bd4a8c
    var args = {
      data: { estado: "ACEPTADO" },
      headers: { "Content-Type": "application/json" }
    };
    var req = client.put("http://" + ip + "/api-usuario/voluntario-organizacion/" + pk_org + "/?pk_voluntario=" + pk_volun, args, function (data, response) {
      if (response.statusCode == '201') {
        var message_notificacion = {
          mensaje: "Su solicitud de voluntario ha sido aceptado",
          pk: pk_volun
        };
        // SOCKET PARA NOTIFICAR ORGANIZACION      
        io.sockets.emit('notificacion', message_notificacion);

        var message_notificacion_org = {
          mensaje: data.response,
          pk: pk_org
        };        
        // SOCKET PARA ENVIAR VALIDACION A VOLUNTARIO
        io.sockets.emit('error', message_notificacion_org);
      }else if (response.statusCode == '404') {          
        var message_notificacion_org = {          
          mensaje: "URL ingresada incorrecta",
          pk: pk_org
        };        
        // SOCKET PARA REENVIAR ERRORES A VOLUNTARIO
        io.sockets.emit('error', message_notificacion_org);
      }else{
        var message_notificacion_org = {          
          mensaje: data.error,
          pk: pk_org
        };        
        // SOCKET PARA REENVIAR ERRORES A VOLUNTARIO
        io.sockets.emit('error', message_notificacion_org);
      }
      
    });
    req.on('error', function (err) {
      io.sockets.emit('error', err);
    });
  });

  socket.on('disconnect', function () {
    console.log('CLIENTE se ha desconectado de Sockets');
    io.emit('user disconnected');
  });
});

/**
 * ############################################################################
 */
app.post('/notificaciones', function (req, res, next) {
  console.log('the response will be sent by the next function ...');
  next();  
}, function (req, res) {   
  sendNotificationToUser(req.body.token,req.body.tipo)   
  res.send("{response:'corrcto'}");
});


var sendNotificationToUser=function(token, tipo) {
  var API_KEY = "AAAAkjR1CDg:APA91bH0H9r4fdCShTa7UCHp1ZPPV81x8Z2T2DAKNfWFEqv8j9Wctb18KxMe0fA26ArFfwKhE9qdzhZ6fMYdMdrIWTmm_PhyWndxFOKkfaT00qvRO5MLPw-jJ_8NZqcIrr2Blj24a11b"; // Your Firebase Cloud Server API key
  
  // Solicitud de Voluntario == 1
  // Aceptacion de Voluntario == 0
  if(tipo==1){
    titulo = "Solicitud de Voluntario"
    mensaje = "Alguien quiere ser voluntario en su organizacion"
  }else{
    titulo = "Aceptacion de Voluntario"
    mensaje = "Han aceptado su solicitud de voluntario"
  }

  request({
    url: 'https://fcm.googleapis.com/fcm/send',
    method: 'POST',
    headers: {
      'Content-Type' :'application/json',
      'Authorization': 'key='+API_KEY
    },
    body: JSON.stringify({
      notification: {        
        title: titulo,
        body:mensaje
      },
      "to" : token
    })
  }, function(error, response, body) {
    if (error) { console.error(error); }
    else if (response.statusCode >= 400) { 
      console.error('HTTP Error: '+response.statusCode+' - '+response.statusMessage); 
    }
    else {      
    }
  });
};

server.listen(3000, function () {
  console.log("Servidor corriendo en http://localho3000");
});