# **TEMPLATE DJANGO DOCKER**

Este template se lo ha construido para crear de manera sencilla un proyecto en django

## La imagen docker está construido con lo siguiente:
- Ubuntu 16 con python3.6
- Django 2.0
- Postgresql
- Otras librerías python.

### Creación de una APP.
    Creación de una app:
        - docker-compose run django_project python3 manage.py startapp <NOMBRE app> <directorio>
        - y vuelva a construir con docker-compose build
        - Ejemplo:
            - Primero cree la carpeta de usuarios sin ningún contenido en el host. Ejem: mkdir apps/usuarios
            - Luego ejecute: docker-compose run django_project python3 manage.py startapp usuarios apps/usuarios
        

## Otras Configuraciones

### Ejecución del Proyecto
    - Si ya están instaladas las librerías en la carpeta vendors ejecute
        - docker-compose up -d django_project
    - Sino ejecute para que se instalen:
        - docker-compose up -d

### Ejecutar Migraciones
    - docker-compose run --rm django_project python3 manage.py makemigrations
    - docker-compose run --rm django_project python3 manage.py migrate

### Ejecución de TESTS
    Asegurese de tener configurado el archivo env_dev para test y sus requerimientos para test en el archivo test.txt
    - docker-compose -f tests.yml build
    - docker-compose -f tests.yml run --rm django_project

### Ejecución En Staging(Pre-Producción)
    Asegurese de tener configurado el archivo env_dev para staging y sus requerimientos para staging en el archivo staging.txt
    - docker-compose -f staging.yml build
    - docker-compose -f staging.yml run --rm django_project

### Ejecución En producción
    Asegurese de tener configurado el archivo env_dev para production y sus requerimientos para producción en el archivo production.txt
    - docker-compose -f production.yml build
    - docker-compose -f production.yml run --rm django_project