"""django_oetec_serve URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from apps.usuarios.usuario.serializers import JSONWebTokenSerializer
from apps.usuarios.usuario.views import ObtainJSONWebToken, DatosCuentaView
from apps.usuarios.voluntario.views import CertificadoViewVoluntario
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

urlpatterns = [
    path('admin/', admin.site.urls),
    
    path('api-usuario/', include('apps.usuarios.urls', namespace="usuarios-oetec")),

    path('api-aisports-auth-token/login', ObtainJSONWebToken.as_view(serializer_class=JSONWebTokenSerializer), name='jwt-create'),
    path('api-aisports-auth-refresh-token/', refresh_jwt_token, name='api-token-refresh'),
    path('api-aisports-verify-token/', verify_jwt_token, name='api-token-verify'),

    path('validation/<token>/<nombreUsuario>', DatosCuentaView.validarCuenta),
    
    re_path('usuario/validar/correo/$', DatosCuentaView.as_view({'post':'validar_email'}) ),

    path('generar-notificacion-certificado/<pk_organizacion>/<pk_voluntario>', CertificadoViewVoluntario.notificarCorreo),
    path('certificado/<pk_organizacion>/<pk_voluntario>', CertificadoViewVoluntario.urlCertificado),
]
