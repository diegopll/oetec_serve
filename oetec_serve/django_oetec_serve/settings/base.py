"""
Django settings for django_oetec_serve project.

Generated by 'django-admin startproject' using Django 2.0.3.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""

import os
import datetime

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'qmd4t&a3yp*b=3)5+f_=n@em4#yx*!z*=ja+zh8)r#o)qtyuzo'

# SECURITY WARNING: don't run with debug turned on in production!
#DEBUG = True

ALLOWED_HOSTS = []


# Application definition

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

OWNER_APPS = [
    'apps.usuarios',
]

THIRD_APPS = [
    'corsheaders',
    'rest_framework',
    'rest_framework_swagger',
    'django_filters',
]

INSTALLED_APPS = DJANGO_APPS + OWNER_APPS + THIRD_APPS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'django_oetec_serve.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['plantillas/'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'django_oetec_serve.wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'usuarios.Usuario'

########################## Configuraciones de CORS
# ALLOW CORS CONFIG
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_HEADERS = (
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'Access-Control-Allow-Headers',
    'Access-Control-Allow-Origin',
    'x-csrftoken',
    'x-requested-with',
)

####################################################
# Clases de Rest Framework para autenticación
# CUSTOM USER MODEL
AUTH_USER_MODEL = 'usuarios.Usuario'

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        # 'rest_framework.permissions.IsAuthenticated',
    ),
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),    
    'EXCEPTION_HANDLER': 'apps.utils.handled_error.custom_handled_error.handled_error'
}

####################################################
# Configuración de libreria JWT para Django:
# jwt Se usa una librería de Json Web Token el cual ya no usa cookies para auteticarse
# es un protocolo donde en cada petición que se realiza se envia un token
JWT_AUTH = {
    'JWT_AUTH_HEADER_PREFIX': 'Bearer',
    'JWT_SECRET_KEY': "55*o=zhdgkt5^t^^+4#(mf4@_j7k%-n*z+o#-4+j0a=2pjqy)(",
    'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=1200000),
    'JWT_ALLOW_REFRESH': True,
    'JWT_RESPONSE_PAYLOAD_HANDLER': 'apps.usuarios.usuario.serializers.jwt_response_payload_handler',
}

# Serializer Personalizados Rest_auth
REST_AUTH_SERIALIZERS = {
    'USER_DETAILS_SERIALIZER': 'apps.usuarios.usuario.serializers.UsuarioSerializer',
    'JWT_SERIALIZER': 'apps.usuarios.usuario.serializers.JWTSerializer',
}

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'es-ec'

TIME_ZONE = 'America/Guayaquil'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'

AUTHENTICATION_BACKENDS = (    
    # Es el Backend de autenticación por defecto que da django
    'django.contrib.auth.backends.ModelBackend',
)

#CONFIGURACION DE CORREO ELECTRONICO
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'pruebasOetec@gmail.com'
EMAIL_HOST_PASSWORD = 'admin1234@'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_BACKEND  =  'django.core.mail.backends.smtp.EmailBackend'