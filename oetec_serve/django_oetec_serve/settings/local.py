
from .base import *
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
#SECRET_KEY = os.environ.get('DJANGO_SECRET_KEY')


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('POSTGRES_DB'),
        'USER': os.environ.get('POSTGRES_USER'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': os.environ.get('DB_PORT')
    },
}

STATIC_URL = '/static/'
STATIC_ROOT = 'staticfiles'

MEDIA_ROOT = 'media'
MEDIA_URL = '/media/'

BASE_URL = 'http://localhost:8050'
BASE_URL_VALIDATION_ACCOUNT = BASE_URL + '/validation/'
URL_REDIRECT = 'https://example.com/'

GENERACION_CERTIFICADO = BASE_URL + '/certificado/'