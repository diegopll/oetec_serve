
"""
Este módulos contiene la forma de validacion de registro de cuenta
"""
from django.core.mail import EmailMessage
from django.conf import settings
from django.template.loader import render_to_string
import base64

class ValidatorResetURL:    
    """
    Clase validacion de cuenta de usuario
    """

    """
    Metodo para envio de correo
    ------- Se recibe email para saber destinatario
    ------- Se recibe username para consulta su estado de cuenta
    ------- Se genera token equivalente a 3 veces tamaño de user
    """
    def envioCorreoPassword(self, nombreUser, cedula, email):
        context = {            
            'usuario': base64.b16encode(bytes(nombreUser, 'utf-8')),
            'cedula': cedula
        }        
        template = render_to_string('resetPasswordForgotten/plantillaEmail.html', context)
        msg = EmailMessage('Oetec Administradores', template, settings.EMAIL_HOST_USER, [email])
        msg.content_subtype = "html"
        msg.send()
        return True  