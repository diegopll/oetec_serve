"""
Este módulos contiene los diferentes validadores para
la app personas.
"""
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.core.validators import validate_email
import datetime

def validar_cedula(valor):
    """
    Esta función sirve para validar la cédula ecuatoriana.
    - verifica que tenga al menos 10 caracteres, y de acuerdo
      al algoritmo de cedula se crea la función.
    """
    if len(valor) <= 9:
        raise ValidationError("La cedula {valor} debe contener al menos 10 caracteres".format(valor=valor))
    cedula = [int(cedula_int) for cedula_int in valor]
    verificador = cedula[9]
    resultado = []
    resultado_coef = 0
    tamano = 9
    i = 0
    decena = 10
    for i in range(tamano):
        if i % 2 == 0:
            resultado.append(cedula[i]*2)
        else:
            resultado.append(cedula[i]*1)
        if resultado[i] > 9:
            resultado[i] = resultado[i] - 9
        resultado_coef = resultado_coef + resultado[i]
    i = 0
    while decena < resultado_coef:
        i += 1
        decena = i * 10
        if decena >= resultado_coef:
            resultado_coef = decena - resultado_coef
            if resultado_coef != verificador:
                raise ValidationError("La cedula {valor} es incorrecta".format(valor=valor))

def validar_email(valor):
    """
    Validador de email: Dado por la clase DjangoCore
    """
    try:
        validate_email(valor)        
    except ValidationError:
        raise ValidationError(_("Email no valido"))    

def validar_fecha_publicacion(valor):
    """
    Validador de email: Dado por la clase DjangoCore
    """
    if(datetime.date.today()<valor):
        raise ValidationError(_("Fecha publicacion invalida"))    

def validar_passwword(valor):           
    if len(valor)>100 or len(valor)<5:
        raise ValidationError(_("Contraseña demasiado corta o larga"))
    return True    

def validar_cadenaString(valor):    
    if len(valor)>100 or len(valor)<3:
        raise ValidationError(_("Campo demasiado corta o larga"))
    return True    

"""
VALIDADOR DE CEDULA O RUC
"""
def verificar_cedula_ruc(nro):
    try:
        l = len(nro)
        if l == 10 or l == 13: # verificar la longitud correcta
            cp = int(nro[0:2])
            if cp >= 1 and cp <= 22: # verificar codigo de provincia
                tercer_dig = int(nro[2])
                if tercer_dig >= 0 and tercer_dig < 6 : # numeros enter 0 y 6
                    if l == 10:
                        return __validar_ced_ruc(nro,0)                       
                    elif l == 13:
                        return __validar_ced_ruc(nro,0) and nro[10:13] != '000' # se verifica q los ultimos numeros no sean 000
                elif tercer_dig == 6:
                    return __validar_ced_ruc(nro,1) # sociedades publicas
                elif tercer_dig == 9: # si es ruc
                    return __validar_ced_ruc(nro,2) # sociedades privadas
                else:
                    raise ValidationError(_(u'Tercer digito invalido'))
            else:
                raise ValidationError(_(u'Codigo de provincia incorrecto'))
        else:
            raise ValidationError(_(u'Longitud incorrecta del numero ingresado'))
    except ValueError:
        raise ValidationError(_(u'Dato ingresado erroneo'))

def __validar_ced_ruc(nro,tipo):
    try:
        total = 0
        if tipo == 0: # cedula y r.u.c persona natural
            base = 10
            d_ver = int(nro[9])# digito verificador
            multip = (2, 1, 2, 1, 2, 1, 2, 1, 2)
        elif tipo == 1: # r.u.c. publicos
            base = 11
            d_ver = int(nro[8])
            multip = (3, 2, 7, 6, 5, 4, 3, 2 )
        elif tipo == 2: # r.u.c. juridicos y extranjeros sin cedula
            base = 11
            d_ver = int(nro[9])
            multip = (4, 3, 2, 7, 6, 5, 4, 3, 2)
        for i in range(0,len(multip)):
            p = int(nro[i]) * multip[i]
            if tipo == 0:
                total+=p if p < 10 else int(str(p)[0])+int(str(p)[1])
            else:
                total+=p
        mod = total % base
        val = base - mod if mod != 0 else 0
        return val == d_ver
    except ValueError:
        raise ValidationError(_(u'Dato ingresado erroneo'))