from django.core.mail import EmailMessage
from django.conf import settings
from django.template.loader import render_to_string
from django.shortcuts import render_to_response
from django.utils.crypto import get_random_string

def envioCorreoUsuario(nombre,dias,fecha_inicio,email):
        context = {
            'nombre': nombre,
            'dias': dias,
            'fecha_inicio': fecha_inicio
        }        
        template = render_to_string('certificado/mensajeEmail.html', context)
        msg = EmailMessage('Solicitud Certificado', template, settings.EMAIL_HOST_USER, [email])
        msg.content_subtype = "html"
        msg.send()
        return True    