from rest_framework.views import exception_handler
import json

def handled_error(exc, context):
    """
    Permite customizar los errores
    en django rest framework.
    primero se obtiene el hadled error de 
    REST framework que viene por defecto para
    obtener el standard error
    """
    response = exception_handler(exc, context)
    if response is not None:
        if response.status_code == 400:
            response.data["error"] = manage_errors(response.data)
    return response

def manage_errors(errors):
    """
    Función recursiva que maneja los errores
    verifica todo el árbol de opciones y devuelve
    el último error.
    """
    message = ""
    if isinstance(errors, list):
        errors = errors[0]
    if len(errors) > 0 and isinstance(errors, list):
        message = manage_errors((errors))
    elif len(errors) > 0 and not isinstance(errors, str):
        #message = manage_errors(list(errors))+":"+manage_errors(list(errors.values()))
        message = manage_errors(list(errors.values()))
    else:
        return errors
    return message