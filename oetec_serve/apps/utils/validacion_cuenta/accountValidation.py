
"""
Este módulos contiene la forma de validacion de registro de cuenta
"""
from django.core.mail import EmailMessage
from django.conf import settings
from django.template.loader import render_to_string
from django.shortcuts import render_to_response
from django.utils.crypto import get_random_string

class AccountValidation:    
    """
    Clase validacion de cuenta de usuario
    """

    """
    Metodo para envio de correo
    ------- Se recibe email para saber destinatario
    ------- Se recibe username para consulta su estado de cuenta
    ------- Se genera token equivalente a 3 veces tamaño de user
    """
    def envioCorreoUsuario(self, email, user):    
        context = {
            'url': settings.BASE_URL_VALIDATION_ACCOUNT,
            'nombre': user,
            'token': get_random_string(len(user)*3)
        }        
        template = render_to_string('registerValidator/plantillaEmail.html', context)
        msg = EmailMessage('Oetec Administradores', template, settings.EMAIL_HOST_USER, [email])
        msg.content_subtype = "html"
        msg.send()
        return True    