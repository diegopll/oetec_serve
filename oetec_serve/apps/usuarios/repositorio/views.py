from rest_framework.response import Response
from rest_framework import viewsets
from apps.usuarios.models import Repositorio, Organizacion
from .serializers import OrganizacionListRepositorioSerializer,RepositorioSerializer
from django.shortcuts import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from django.http import QueryDict
from rest_framework import status
from django.core.exceptions import ValidationError
import datetime
from django.utils import timezone

class Pagination(PageNumberPagination):
    """
    Paginación para la Lista de personas
    """
    page_size = 20

    def get_paginated_response(self, data):
        return Response({
            'next': self.page.next_page_number() if self.page.has_next() else 0,
            'previous': self.page.previous_page_number() if self.page.has_previous() else 0,
            'count': self.page.paginator.count,
            'results': data
        })

class RepositorioPersonalizadoView(viewsets.ReadOnlyModelViewSet):
    """
    REPOSITORIOS DE ORGANIAZION SEGUN PK
    """    
    pagination_class = Pagination    
    serializer_class = OrganizacionListRepositorioSerializer    
    queryset =  Repositorio.objects.all().order_by('titulo')        
            
    def retrieve(self, request, pk=None):
        """
        OBTIENE TODOS LOS REPOSITORIOS DE LA ORGANIZACION SEGUN SU PK O ID
            param: self
            param: request
            param: pk de organizacion
        """        
        organizacion = get_object_or_404(Organizacion.objects.all(), pk=pk)
        queryset = Repositorio.objects.filter(organizacion=organizacion).order_by('titulo')        
        datosModificados = ""        
        paginacion = ""

        for key, value in request.query_params.items():
            if(value):
                if(key=='titulo'):
                    datosModificados += key+"__icontains="+value+"&&"
                if(key=='categoria'):
                    datosModificados += key+"__nombre__icontains="+value+"&&"
                if(key=='fecha_publicacion'):
                    datosModificados += key+"="+value+"&&"
                if(key=='page'):
                    paginacion= key+"="+value

        paramsURL = QueryDict(datosModificados)
        pageURL = QueryDict(paginacion)
        try:
            if "page" in pageURL:
                queryset = queryset.filter(**paramsURL.dict())
                queryset = self.paginate_queryset(queryset) 
                serializer = OrganizacionListRepositorioSerializer(queryset, many=True, context={"request": request})
                return self.get_paginated_response(serializer.data)

            queryset = queryset.filter(**paramsURL.dict())
            serializer = OrganizacionListRepositorioSerializer(queryset, many=True, context={"request": request})
            return Response(serializer.data,status=status.HTTP_200_OK)  
        except ValidationError:
            return Response({"error": 'Ingrese datos validos en el filtro'},status=status.HTTP_400_BAD_REQUEST)  


class RepositorioView(viewsets.ModelViewSet):
    """
    VIEW PARA CREAR Y EDITAR REPOSITORIO
    """    
    pagination_class = Pagination    
    serializer_class = RepositorioSerializer    
    queryset =  Repositorio.objects.all().order_by('titulo')        

    def create(self, request):
        serializer = RepositorioSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):            
            serializer.save()                        
            return Response({"response": 'Guardado Correctamente'},status=status.HTTP_201_CREATED)
        return Response(serializer.erros,status=status.HTTP_400_BAD_REQUEST) 

    def update(self, request, pk=None):
        """        
            :param self: 
            :param request: 
            :param pk=None: 
        """            
        try:
            queryset = get_object_or_404(Repositorio.objects.all(), pk=pk)
            serializer = RepositorioSerializer(queryset,data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()            
                return Response({"response": 'Se ha actuailzado correctamente'},status=status.HTTP_201_CREATED)
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)    
        except Repositorio.DoesNotExist:
            return Response({"error": 'No existe repositorio con ese pk'}, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError:
            return Response({"error": 'Ha ingresado pk repositorio no valido'}, status=status.HTTP_400_BAD_REQUEST)
        

    def retrieve(self, request, pk=None):
        try:        
            queryset = Repositorio.objects.all()            
            user = get_object_or_404(queryset, pk=pk)
            
            # SEE EJECUTA CUANDO ACTUALIZA O ACCEDEN A VER EL DETALLE DEL REPOSITORIO
            user.fecha_acceso = datetime.datetime.now(tz=timezone.utc)
            user.save()

            serializer = RepositorioSerializer(user)
            return Response(serializer.data,status=status.HTTP_200_OK)
        except Repositorio.DoesNotExist:
            return Response({"error": 'No existe repositorio con ese pk'}, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError:
            return Response({"error": 'Ha ingresado pk repositorio no valido'}, status=status.HTTP_400_BAD_REQUEST)