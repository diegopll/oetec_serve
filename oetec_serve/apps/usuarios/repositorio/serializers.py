from rest_framework import serializers
from apps.usuarios.models import Repositorio
import datetime
from django.utils import timezone

class OrganizacionListRepositorioSerializer(serializers.ModelSerializer):
    """
    
    """    
    categoria = serializers.CharField(source="categoria.nombre")
    pk_categoria = serializers.CharField(source="categoria.pk")

    class Meta:
        model = Repositorio
        fields = (    
            'pk',
            'titulo',
            'url',
            "descripcion",
            'fecha_publicacion', 
            'fecha_acceso',
            'categoria',         
            'pk_categoria'
        )        

class RepositorioSerializer(serializers.ModelSerializer):
    """
    
    """        
    class Meta:
        model = Repositorio
        fields = (            
            'titulo',
            'url',
            "descripcion",
            'fecha_publicacion',             
            'organizacion',
            'categoria'           
        )

    def create(self, validated_data):
        Repositorio.objects.create(**validated_data, fecha_acceso=datetime.datetime.now(tz=timezone.utc))        
        return validated_data
    
    def update(self, instance, validated_data):        
        instance.titulo = validated_data.get('titulo')
        instance.url=validated_data.get('url')
        instance.descripcion =validated_data.get('descripcion')
        instance.fecha_publicacion=validated_data.get('fecha_publicacion')        
        instance.organizacion=validated_data.get('organizacion')
        instance.categoria=validated_data.get('categoria')
        instance.fecha_acceso = datetime.datetime.now(tz=timezone.utc)
        instance.save()
        return instance

    def validate(self, validated_data):                
        return validated_data