from rest_framework import serializers
from apps.usuarios.models import Usuario
from rest_framework_jwt import serializers as jwt_serializers
from django.core.validators import ValidationError
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import authenticate
from rest_framework_jwt.settings import api_settings
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.hashers import make_password
from apps.utils.reset_password.validatorResetURL import ValidatorResetURL

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

class UsuarioSerializer(serializers.ModelSerializer):
    """
    Recursos del Usuario
    """
    pk_perfil = serializers.SerializerMethodField()
    
    class Meta:
        model = Usuario
        fields = (
            'pk',
            'email',            
            'is_active',       
            'pk_perfil',
            'tipo_usuario',
            'token_firebase'
        )
    
    def get_pk_perfil(self, obj):
        """
        obtiene el id del perfil
        """
        try:
            if obj.tipo_usuario == "ORGANIZACION":
                return obj.organizacion.pk
            elif obj.tipo_usuario == "VOLUNTARIO":
                return obj.voluntario.pk
            return None
        except ObjectDoesNotExist:
            return "El usuario no tiene asignado un perfil"

def jwt_response_payload_handler(token, user=None, request=None):
    """
    Respuesta al devolver el JWT.
    """
    user = Usuario.objects.get(email=request.data.get('email'))
    user.token_firebase = request.data.get('token')
    user.save()
    return {
        'token': token,
        'user': UsuarioSerializer(user, context={'request': request}).data
    }

class JWTSerializer(serializers.Serializer):
    """
    Serializer para autenticación con JWT
    """
    token = serializers.CharField()
    user = UsuarioSerializer()


class JSONWebTokenSerializer(jwt_serializers.JSONWebTokenSerializer):
    """
    Override rest_framework_jwt's ObtainJSONWebToken serializer to 
    force it to raise ValidationError exception if validation fails.
    """    
    
    def validate(self, attrs):
        credentials = {
            self.username_field: attrs.get(self.username_field),
            'password': attrs.get('password')
        }
        user = None
        
        if all(credentials.values()):
            
            exist = Usuario.objects.filter(email=attrs.get('email')).exists()
            if exist:   
                usuario = Usuario.objects.get(email=attrs.get('email'))
                if not usuario.is_active:
                    raise ValidationError(_("El usuario aún no está activo"))
            else:
                raise ValidationError(_("Credenciales inválidas"))
            user = authenticate(email=usuario.email, password=attrs.get('password'))
            if user:
                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg)

                payload = jwt_payload_handler(user)

                return {
                    'token': jwt_encode_handler(payload),
                    'user': user
                }
            else:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "{username_field}" and "password".')
            msg = msg.format(username_field=self.username_field)
            raise serializers.ValidationError(msg)

class UsuarioValidateSerializers(serializers.ModelSerializer):
    """
    Recursos del Usuario
    """
    class Meta:
        model = Usuario
        fields = (            
            'email',            
            'password',
        )

    def validate(self, validated_data):
        """
        VALIDADORES DE DATOS
        """        
        return validated_data

class UsuarioUpdateSerializers(serializers.ModelSerializer):
    """
    Recursos del Usuario
    """
    class Meta:
        model = Usuario
        fields = (                      
            'email',            
        )

    def validate(self, validated_data):
        """
        VALIDADORES DE DATOS
        """        
        return validated_data

class CambioClaveUsuarioSerializer(serializers.ModelSerializer):
    """
    SERIALIZER SOBRE LOS VOLUNTARIOS DE LA ORGANIZACION
        ESTADOS = ACEPTADO __ NO_ACEPTADO
    """             
    class Meta:
        model = Usuario
        fields = (  
            'password',    
        )

    def update(self, instance, validated_data):
        validated_data['password'] = make_password(validated_data.get('password'))
        instance.password = validated_data.get('password')
        instance.save()
        return instance

class UsuarioEmailModelSerializers(serializers.ModelSerializer):
    """
    Serializador de username de Usuario
    """
    email = serializers.CharField(max_length=255)

    class Meta:
        model = Usuario
        fields = (       
            'email',
        )
    
    def validate(self, values):
        """
        Metodo de validacion
            :param self: Por defecto
            :param values: Parametros a recibir desde vista
        """             
        cedula = ''
        emailUser = values.pop('email')
        usuario = Usuario.objects.get(email= emailUser)        
        usuario.validate_isActive()
        if (usuario.tipo_usuario=='ORGANIZACION'):
            usuario.password = make_password(usuario.organizacion.cedula_ruc)
            cedula = usuario.organizacion.cedula_ruc
        else:
            usuario.password = make_password(usuario.voluntario.cedula_ruc)
            cedula = usuario.voluntario.cedula_ruc
        usuario.save()
                        
        urlResetPassword = ValidatorResetURL()
        urlResetPassword.envioCorreoPassword(emailUser, cedula, emailUser)
        return values