from rest_framework_jwt.views import ObtainJSONWebToken
from rest_framework_jwt import views as jwt_views
from rest_framework import viewsets
from .serializers import (
    JSONWebTokenSerializer, jwt_response_payload_handler, JSONWebTokenSerializer,CambioClaveUsuarioSerializer,UsuarioEmailModelSerializers)
from rest_framework.response import Response
from django.conf import settings
from rest_framework import generics, status
from apps.usuarios.models import Organizacion, Usuario, Voluntario
from django.shortcuts import HttpResponse, render, redirect
from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404

class ObtainJSONWebToken(jwt_views.ObtainJSONWebToken):
    """
    Override the default JWT ObtainJSONWebToken view to use the custom serializer
    """
    
    def post(self, request):
        if(request.data.get('token')):
            serializer = self.get_serializer(
                data=request.data
            )
            serializer.is_valid(raise_exception=True) # pass the 'raise_exception' flag
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            response_data = jwt_response_payload_handler(token, user, request)
            return Response(response_data)
        else:
            return Response({"error": 'No ha enviado pk de firebase'}, status=status.HTTP_400_BAD_REQUEST)

class DatosCuentaView(viewsets.ViewSet):

    def validarCuenta(self, token, nombreUsuario):
            """
            URL para validacion de usuario
            ------- Se recibe token que contiene el 3 veces el tamaño del nombreUsario: servira como comprobacion Extra
            ------- Se recibe nombre de usuario para verificar si existe usuario en bd 
            ------- Url de validacion solo podra ser usado 1 sola vez
            """         
            try:            
                nombreOrganizacion = str(nombreUsuario)
                if(len(token)/3) == len(nombreUsuario):
                    cuenta = ""
                    try:
                        cuenta = Organizacion.objects.get(nombre=nombreOrganizacion)
                    except Organizacion.DoesNotExist:
                        cuenta = Voluntario.objects.get(nombre=nombreOrganizacion)
                    user = Usuario.objects.get(pk= cuenta.usuario.pk)
                    if user.is_active:                                
                        return redirect(settings.URL_REDIRECT)
                    else:                                    
                        user.is_active = True
                        user.save()                                 
                        return render(self,'registerValidator/validationAccount.html')
                else:
                    #return Response('ERROR DE ACCESO NO AUTORIZADO',status=status.HTTP_401_UNAUTHORIZED)
                    return redirect(settings.URL_REDIRECT)
            except Usuario.DoesNotExist:
                return Response({"error": 'Cuenta no existe en Dsafio'},status=status.HTTP_404_NOT_FOUND)

    def validar_email(self, request):
        """
        Metodo para validar si email existe y si esta activo
            :param self: Por defecto
            :param request: Parametros recibidos por POST
        """   
        try:
            serializer = UsuarioEmailModelSerializers(data=request.data)        
            if (serializer.is_valid(raise_exception=True)):
                return Response({"response": 'Email Valido por favor verifica tu correo'},status=status.HTTP_202_ACCEPTED)
            else:
                return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
        except Usuario.DoesNotExist:
            return Response({"error": 'Su email no existe en Dsafio'}, status=status.HTTP_400_BAD_REQUEST)

class CambioClaveUsuarioView(viewsets.ViewSet):    

    def update(self, request, pk=None):
        """        
            :param self: 
            :param request: 
            :param pk=None: 
        """          
        try:  
            queryset = get_object_or_404(Usuario.objects.all(), pk=pk)
            serializer = CambioClaveUsuarioSerializer(queryset,data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()            
                return Response({"response": 'Se ha actuailzado correctamente'},status=status.HTTP_201_CREATED)
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)    
        except Voluntario.DoesNotExist:
            return Response({"error": 'No existe voluntario con ese pk'}, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError:
            return Response({"error": 'Ha ingresado pk voluntario no valido'}, status=status.HTTP_400_BAD_REQUEST)