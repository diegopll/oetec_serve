from django.db import models
from django.contrib.auth.models import BaseUserManager


# CREACIÓN DEL MODELO USER PERSONALIZADO
class UserManager(BaseUserManager, models.Manager):
    """
    Manager Usuario realiza operaciones de creación
    a traves del modelo.
    """

    def _create_user(self, email, is_staff, is_superuser, password=None,
                     **extra_fields):
        """
        Crear usuario y superusuarios.
        :param email: email requerido de usuario, se ingresa al
                      sistema con el email en lugar del username.
        :param username: username opcional del usuario.
        :param is_staff: Valor booleano. Designa si este usuario
                         puede acceder al sitio administrativo.
        :param is_superuser: Designa que el usuario tiene todos,
                             los permisos sin explicitamente
                             asignarlos.
        :param password: valor requerido del usuario.
        :return: Retorna un usuario guardado.
        """
        email = self.normalize_email(email)
        if not email:
            raise ValueError('Los Usuarios deben tener un email')
        user = self.model(
            email=email,            
            is_active=True,
            is_staff=is_staff,
            is_superuser=is_superuser,
            **extra_fields
        )
        user.set_password(password)  # para hash del password
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """
        Crear y guardar un Usuario.
        """
        return self._create_user(email, False, False, password,
                                 **extra_fields)

    def create_superuser(self, email, password,
                         **extra_fields):
        """
        Crear y guardar Super Usuarios
        """
        return self._create_user(email, True, True, password,
                                 **extra_fields)