# Register your models here.
from django.contrib import admin
from django import forms

from apps.usuarios.models import (
    Usuario,Voluntario,Organizacion,Voluntario_organizacion,Categoria,Red_social,Repositorio,Rendicion_cuentas)
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group, Permission
from django.contrib.auth.forms import ReadOnlyPasswordHashField

# Register your models here.


class UserCreationForm(forms.ModelForm):
    """
    Un form para crear nuevos usuarios. Incluye todos los campos
    requeridos, mas un repetidor de password.
    """
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = Usuario
        fields = ('email', )                

    def clean_password2(self):
        """
        Chequea que los dos password ingresen.
        """
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        """
        Guarda y provee al password un hash
        """
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """
    Un form  para actualizar usuarios. Incluye todos los
    campos del usuario pero reemplaza el campo password con el
    password hasheado del admin.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Usuario
        fields = ('email', 'password', 'is_active', 'is_staff', 'tipo_usuario')

    def clean_password(self):
        """
        Independientemente de lo que el usuario proporcione,
        devuelva el valor inicial.
        """
        return self.initial["password"]


class UserAdmin(BaseUserAdmin):
    """
    form para añadir una instancia de usuario
    """
    form = UserChangeForm
    add_form = UserCreationForm
    # inlines = [UsuarioInline]

    list_display = ('email', 'is_staff', 'tipo_usuario')
    list_filter = ('is_staff',)

    fieldsets = (
        (None, {'fields': ('email',)}),
        ('Personal info', {'fields': ('password', 'tipo_usuario')}),
        ('Permissions', {'fields': ('is_staff', 'is_active', 'groups', 'user_permissions')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'tipo_usuario')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()

admin.site.register(Usuario,UserAdmin)
admin.site.register(Organizacion)
admin.site.register(Voluntario)
admin.site.register(Voluntario_organizacion)
admin.site.register(Categoria)
admin.site.register(Red_social)
admin.site.register(Repositorio)
admin.site.register(Rendicion_cuentas)