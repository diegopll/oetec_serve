from rest_framework import serializers
from apps.usuarios.models import Organizacion, Usuario, Red_social
from apps.usuarios.usuario.serializers import UsuarioValidateSerializers
from apps.usuarios.red_social.serializers import RedSocialSerializers
from apps.utils.validacion_cuenta.accountValidation import AccountValidation
from django.contrib.auth.hashers import make_password
from apps.utils.validadores.validadores import validar_email
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

class OrganizacionSerializer(serializers.ModelSerializer):
    """
    RECURSO ORGANIZACION
    """
    usuario = UsuarioValidateSerializers()

    class Meta:
        model = Organizacion
        fields = (            
            'nombre',            
            "cedula_ruc",
            "representante",                        
            'usuario'
        )

    def create(self, validated_data):
        """
        METODO PARA CREAR ORGANIZACION
            param: self
            param: validated_data 
        """
        datos_usuario = validated_data.pop('usuario')  
        datos_usuario['password'] = make_password(datos_usuario.get('password'))        
        usuario = Usuario.objects.create(**datos_usuario,tipo_usuario='ORGANIZACION')
        organizacion = Organizacion.objects.create(**validated_data,usuario=usuario,url='',telefono='',descripcion='')
        Red_social.objects.create(nombre='FACEBOOK',url='',organizacion=organizacion)
        Red_social.objects.create(nombre='TWITER',url='',organizacion=organizacion)
        AccountValidation.envioCorreoUsuario(self,datos_usuario.get('email'),validated_data.get('nombre'))        
        return validated_data

    def update(self, instance, validated_data):        
        return instance

    def validate(self, validated_data):
        return validated_data

class OrganizacionListSerializer(serializers.ModelSerializer):
    """
    RECURSO LISTAS DE ORGANIZACION
    """    
    urldetalle = serializers.HyperlinkedIdentityField(view_name='api-app-usuarios:organizacion-detail')

    class Meta:
        model = Organizacion
        fields = (
            'pk',
            'nombre',            
            "descripcion",
            'urldetalle'
        )

class OrganizacionDetailSerializer(serializers.ModelSerializer):
    """
    RECURSO DETALLE DE ORGANIZACION
    """        
    red_social = serializers.JSONField(source="obtener_red_social")
    token_firebase = serializers.CharField(source='usuario.token_firebase')
    email = serializers.CharField(source='usuario.email')
    estado = serializers.SerializerMethodField()

    class Meta:
        model = Organizacion
        fields = (            
            'pk',
            'nombre',
            'url',
            'email',
            "cedula_ruc",
            "representante",
            "telefono",
            "descripcion",
            'red_social',
            'token_firebase',
            'estado'
        )
    
    def get_estado(self, obj):
        """
        OBTIENE EL ESTADO DE LA RELACION INTERMEDIA
        SEGUN EL PK DE LA ORGANIZACION
        """
        try:
            voluntario = self.context["pk_voluntario"]
            print(voluntario)
            return obj.get_estado(voluntario)
        except KeyError:
            return "ANONIMO"        

class OrganizacionUpdateSerializers(serializers.ModelSerializer):
    """
    RECURSO ORGANIZACION
    """    
    red_social = RedSocialSerializers(many=True)
    email = serializers.CharField(source="usuario.email")

    class Meta:
        model = Organizacion
        fields = (            
            'nombre',
            'url',
            "cedula_ruc",
            "representante",
            "telefono",
            "descripcion",
            'red_social',
            'email'            
        )
    
    def update(self, instance, validated_data):        
        usuario_email = validated_data.pop('usuario')
        red_sociales = validated_data.pop('red_social')
        
        instance.nombre = validated_data.get('nombre')
        instance.url = validated_data.get('url')
        instance.cedula_ruc = validated_data.get('cedula_ruc')
        instance.representante = validated_data.get('representante')
        instance.telefono= validated_data.get('telefono')
        instance.descripcion= validated_data.get('descripcion')

        redes = instance.red_social.all()
        if redes:            
            for r in redes:
                for redesUrl in red_sociales:                    
                    if r.nombre == redesUrl.get('nombre').upper():
                        r.url = redesUrl.get('url')
                        r.save()                
        
        usuario = instance.usuario
        usuario.email = usuario_email.get('email')
        usuario.save()
        
        instance.save()

        return instance

    def validate(self, validated_data):        
        """
        METODO PARA VALIDAR DETERMINADOS CAMPOS DEL SERIALIZADOR
            param: self
            param: validated_data
        """
        validar_email(validated_data.get('usuario')['email'])                    
        return validated_data