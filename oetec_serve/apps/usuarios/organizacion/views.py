from django.shortcuts import get_object_or_404
from .serializers import ( 
    OrganizacionSerializer,OrganizacionListSerializer,OrganizacionDetailSerializer,OrganizacionUpdateSerializers
)
from rest_framework import status
from apps.usuarios.models import Organizacion
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination
from django.core.exceptions import ValidationError

class Pagination(PageNumberPagination):
    """
    Paginación para la Lista de personas
    """
    page_size = 20

    def get_paginated_response(self, data):
        return Response({
            'next': self.page.next_page_number() if self.page.has_next() else 0,
            'previous': self.page.previous_page_number() if self.page.has_previous() else 0,
            'count': self.page.paginator.count,
            'results': data
        })

class OrganizacionView(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving users.
    """
    pagination_class = Pagination    
    serializer_class = OrganizacionSerializer
    queryset =  Organizacion.objects.all()

    def create(self, request):
        serializer = OrganizacionSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):            
            serializer.save()            
            return Response({"response": 'Guardado Correctamente'},status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)   

    def list(self, request):
        """
        Lista API para recurso de Organizaciones
        """
        queryset = Organizacion.objects.all()        
        if "page" in request.query_params:
            queryset = self.paginate_queryset(queryset)
            serializer = OrganizacionListSerializer(queryset, many=True, context={"request": request})
            return self.get_paginated_response(serializer.data)
        serializer = OrganizacionListSerializer(queryset, many=True, context={"request": request})
        return Response(serializer.data,status=status.HTTP_200_OK)

    def retrieve(self, request, pk=None):
        try:
            queryset = get_object_or_404(Organizacion.objects.all(), pk=pk)
            if "pk_voluntario" in request.query_params:                
                serializer = OrganizacionDetailSerializer(queryset,context={"pk_voluntario": request.query_params.get('pk_voluntario')})
                return Response(serializer.data,status=status.HTTP_200_OK) 
            else:
                serializer = OrganizacionDetailSerializer(queryset)
                return Response(serializer.data,status=status.HTTP_200_OK) 
        except Organizacion.DoesNotExist:
            return Response({"error": 'No existe oraganizacion con ese pk'}, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError:
            return Response({"error": 'Ha ingresado pk oraganizacion no valido'}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        """        
            :param self: 
            :param request: 
            :param pk=None: 
        """          
        try:  
            queryset = get_object_or_404(Organizacion.objects.all(), pk=pk)
            serializer = OrganizacionUpdateSerializers(queryset,data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()            
                return Response({"response": 'Se ha actuailzado correctamente'},status=status.HTTP_201_CREATED)
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)    
        except Organizacion.DoesNotExist:
            return Response({"error": 'No existe oraganizacion con ese pk'}, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError:
            return Response({"error": 'Ha ingresado pk oraganizacion no valido'}, status=status.HTTP_400_BAD_REQUEST)

  
       