from rest_framework import viewsets
from apps.usuarios.models import Categoria
from .serializers import CategoriaSerializer

class CategoriaListView(viewsets.ReadOnlyModelViewSet):
    """
    REPOSITORIOS DE ORGANIAZION SEGUN PK
    """        
    serializer_class = CategoriaSerializer    
    queryset =  Categoria.objects.all().order_by('nombre') 