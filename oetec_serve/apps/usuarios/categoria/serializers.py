from rest_framework import serializers
from apps.usuarios.models import Categoria

class CategoriaSerializer(serializers.ModelSerializer):
    """
    
    """        
    class Meta:
        model = Categoria
        fields = (                        
            'pk',
            'nombre',   
        )