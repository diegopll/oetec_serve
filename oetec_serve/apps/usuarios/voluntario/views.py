from rest_framework.response import Response
from rest_framework import viewsets, status
from apps.usuarios.models import Voluntario, Organizacion, Voluntario_organizacion
from .serializers import OrganizacionLisVoluntariosoSerializer,VoluntariosDetailSerializer, AceptarVoluntarioSerializer,SolicitudVoluntarioSerializer,VoluntarioSerializer,VoluntarioUpdateSerializer
from django.shortcuts import get_object_or_404,render
from django.template.loader import render_to_string
from rest_framework.pagination import PageNumberPagination
from django.http import QueryDict
from django.core.exceptions import ValidationError
from apps.utils.certificado.generarCertificado import envioCorreoUsuario
from rest_framework.decorators import api_view
import datetime

class Pagination(PageNumberPagination):
    """
    Paginación para la Lista de personas
    """
    page_size = 20

    def get_paginated_response(self, data):
        return Response({
            'next': self.page.next_page_number() if self.page.has_next() else 0,
            'previous': self.page.previous_page_number() if self.page.has_previous() else 0,
            'count': self.page.paginator.count,
            'results': data
        })

class VoluntarioOrganizacionView(viewsets.ReadOnlyModelViewSet):
    """
    VISTA DE VOLUNTARIO PARA OBTENER LISTA DE VOLUNTARIOS DE ORGANIZACION
    """    
    pagination_class = Pagination    
    serializer_class = OrganizacionLisVoluntariosoSerializer
    queryset =  Voluntario.objects.all().order_by('nombre')

    def retrieve(self, request, pk=None):
        """
        OBTIENE TODOS LOS VOLUNTARIOS DE LA ORGANIZACION SEGUN SU PK O ID DE LA ORGANIZACION
        SE OBTIENE VOLUNTARIOS CON SU RESPECTIVO ESTADO
            param: self
            param: request
            param: pk de organizacion
        """
        try:            
            organizacion = get_object_or_404(Organizacion.objects.all(), pk=pk)
        except ValidationError:
            return Response({"error": 'Ha ingresado pk de organizacion no valido'}, status=status.HTTP_400_BAD_REQUEST)
        queryset = Voluntario.objects.filter(organizacion=organizacion).order_by('nombre')
        if "page" in request.query_params:
            queryset = self.paginate_queryset(queryset)
            serializer = OrganizacionLisVoluntariosoSerializer(queryset, many=True, context={"pk_organizacion": pk})
            return self.get_paginated_response(serializer.data)
        
        if "pk_voluntario" in request.query_params:
            try:
                paramsURL = QueryDict('pk='+request.query_params.get('pk_voluntario'))            
                queryset = queryset.get(**paramsURL.dict())
                serializer = VoluntariosDetailSerializer(queryset, context={"pk_voluntario": pk})
                return Response(serializer.data)
            except Voluntario.DoesNotExist:
                return Response({"error": 'No existe voluntario con ese pk'}, status=status.HTTP_400_BAD_REQUEST)
            except ValidationError:
                return Response({"error": 'Ha ingresado pk voluntario no valido'}, status=status.HTTP_400_BAD_REQUEST)
        
        serializer = OrganizacionLisVoluntariosoSerializer(queryset, many=True, context={"pk_voluntario": pk})
        return Response(serializer.data) 

    def update(self, request, pk=None):
        """
        OBTIENE TODOS LOS VOLUNTARIOS DE LA ORGANIZACION SEGUN SU PK O ID DE LA ORGANIZACION
        SE OBTIENE VOLUNTARIOS CON SU RESPECTIVO ESTADO
            param: self
            param: request
            param: pk de organizacion
        """        
        if "pk_voluntario" in request.query_params:
            try:
                queryset = get_object_or_404(Voluntario_organizacion.objects.all(), organizacion=pk,voluntario=request.query_params.get('pk_voluntario'))
                serializer = AceptarVoluntarioSerializer(queryset,data=request.data)
                if serializer.is_valid(raise_exception=True):
                    serializer.save()
                    return Response({"response": 'Solicitud de voluntario Aceptada'},status=status.HTTP_201_CREATED)
                return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)    
            except Voluntario.DoesNotExist:
                return Response({"error": 'No existe usuario con ese pk'}, status=status.HTTP_400_BAD_REQUEST)
            except ValidationError:
                return Response({"error": 'Ha ingresado pk no valido'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"error": 'No ha ingresado en la url el pk de voluntario'}, status=status.HTTP_400_BAD_REQUEST)

class CertificadoViewVoluntario(viewsets.ViewSet):

    @api_view(['GET', 'POST', ])
    def notificarCorreo(self,pk_organizacion,pk_voluntario):
        try:
            organizacion = Organizacion.objects.get(pk = pk_organizacion)
            voluntario = Voluntario.objects.get(pk = pk_voluntario)
            relacion = Voluntario_organizacion.objects.get(organizacion=pk_organizacion,voluntario=pk_voluntario)
            if (relacion.estado=='ACEPTADO'):
                if(relacion.fecha_registro_voluntario):
                    if(datetime.date.today().month-relacion.fecha_registro_voluntario.month)>0:
                        nombresCommpletos =voluntario.nombre +" "+voluntario.apellido
                        dias = str(abs(datetime.date.today() - relacion.fecha_registro_voluntario).days)
                        envioCorreoUsuario(nombresCommpletos,dias,str(relacion.fecha_registro_voluntario),organizacion.usuario.email)                
                        return Response({"response": 'Se ha enviado su solicitud de certificado'},status=status.HTTP_201_CREATED)                
                    else:
                        return Response({"error": 'Para obtener un certificado debe ser voluntario al menos 1 mes'}, status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"error": 'Hubo un problema no tiene fecha de aceptacion de solicitud voluntario'}, status=status.HTTP_400_BAD_REQUEST)
            else:                                
                return Response({"error": 'No ha sido aceptado como voluntario'}, status=status.HTTP_400_BAD_REQUEST)
        except Organizacion.DoesNotExist: 
            return Response({"error": 'No existe organizacion con es id'}, status=status.HTTP_400_BAD_REQUEST)
        except Voluntario.DoesNotExist: 
            return Response({"error": 'No existe voluntario con es id'}, status=status.HTTP_400_BAD_REQUEST)
        except Voluntario_organizacion.DoesNotExist: 
            return Response({"error": 'No ha pedido ser voluntario de la organizacion'}, status=status.HTTP_400_BAD_REQUEST)

    @api_view(['GET', 'POST', ])
    def urlCertificado(self,pk_organizacion,pk_voluntario):
        try:
            organizacion = Organizacion.objects.get(pk = pk_organizacion)
            voluntario = Voluntario.objects.get(pk = pk_voluntario)
            relacion = Voluntario_organizacion.objects.get(organizacion=pk_organizacion,voluntario=pk_voluntario)
            print(voluntario.nombre + voluntario.apellido)
            context = {
                'empresa': organizacion.nombre,
                'voluntario': voluntario.nombre + "  " +voluntario.apellido,
                'meses': datetime.date.today().month-relacion.fecha_registro_voluntario.month,
                'representante': organizacion.representante
            }        
            return render(self,'certificado/certificadoPDF.html',context)
        except Organizacion.DoesNotExist: 
            return Response({"error": 'No existe organizacion con es id'}, status=status.HTTP_400_BAD_REQUEST)
        except Voluntario.DoesNotExist: 
            return Response({"error": 'No existe voluntario con es id'}, status=status.HTTP_400_BAD_REQUEST)
        except Voluntario_organizacion.DoesNotExist: 
            return Response({"error": 'No ha pedido ser voluntario de la organizacion'}, status=status.HTTP_400_BAD_REQUEST)        

class AceptarVoluntarioView(viewsets.ViewSet):
    """
    METODO ES SOLICITAR SER VOLUNTARIO
    """        
    pagination_class = Pagination    
    serializer_class = SolicitudVoluntarioSerializer
    queryset =  Voluntario.objects.all().order_by('nombre')

    def create(self, request):
        serializer = SolicitudVoluntarioSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):            
            serializer.save()                                    
            return Response({"response": 'Solicitud Enviada'},status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)   


class VoluntarioView(viewsets.ModelViewSet):
    """
    VISTA ACEPTACION DE VOLUNTARIO
    """        
    pagination_class = Pagination    
    serializer_class = VoluntarioSerializer
    queryset =  Voluntario.objects.all().order_by('nombre')

    def create(self, request):
        serializer = VoluntarioSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):            
            serializer.save()                                    
            return Response({"response": 'Guardado Correctamente'},status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)   

    def retrieve(self, request, pk=None):
        try:
            queryset = get_object_or_404(Voluntario.objects.all(), pk=pk)
            serializer = VoluntariosDetailSerializer(queryset)
            return Response(serializer.data,status=status.HTTP_200_OK) 
        except Voluntario.DoesNotExist:
            return Response({"error": 'No existe voluntario con ese pk'}, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError:
            return Response({"error": 'Ha ingresado pk voluntario no valido'}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        """        
            :param self: 
            :param request: 
            :param pk=None: 
        """          
        try:  
            queryset = get_object_or_404(Voluntario.objects.all(), pk=pk)
            serializer = VoluntarioUpdateSerializer(queryset,data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()            
                return Response({"response": 'Se ha actuailzado correctamente'},status=status.HTTP_201_CREATED)
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)    
        except Voluntario.DoesNotExist:
            return Response({"error": 'No existe voluntario con ese pk'}, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError:
            return Response({"error": 'Ha ingresado pk voluntario no valido'}, status=status.HTTP_400_BAD_REQUEST)