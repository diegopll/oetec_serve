from rest_framework import serializers
from apps.usuarios.models import Voluntario, Voluntario_organizacion, Usuario, Notificaciones
from apps.usuarios.usuario.serializers import UsuarioValidateSerializers
import datetime 
from apps.utils.validacion_cuenta.accountValidation import AccountValidation
from django.contrib.auth.hashers import make_password
from apps.utils.validadores.validadores import validar_email
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError

class OrganizacionLisVoluntariosoSerializer(serializers.ModelSerializer):
    """
    SERIALIZER SOBRE LOS VOLUNTARIOS DE LA ORGANIZACION
        ESTADOS = ACEPTADO __ NO_ACEPTADO
    """           
    estado = serializers.SerializerMethodField()

    class Meta:
        model = Voluntario
        fields = (  
            'pk',       
            'nombre',
            'apellido',            
            "genero",
            'profesion',  
            'estado'          
        )
    
    def get_estado(self, obj):
        """
        OBTIENE EL ESTADO DE LA RELACION INTERMEDIA
        SEGUN EL PK DE LA ORGANIZACION
        """
        try:
            organizacion = self.context["pk_voluntario"]
            return obj.get_estado(organizacion)
        except KeyError:
            return "No ha enviado pk de organizacion"

class VoluntariosDetailSerializer(serializers.ModelSerializer):
    """
    SERIALIZER SOBRE LOS VOLUNTARIOS DE LA ORGANIZACION
        ESTADOS = ACEPTADO __ NO_ACEPTADO
    """             
    estado = serializers.SerializerMethodField()
    email = serializers.CharField(source="usuario.email")
    token_firebase = serializers.CharField(source='usuario.token_firebase')

    class Meta:
        model = Voluntario
        fields = (  
            'pk',       
            'nombre', 
            'email',
            'apellido',
            'cedula_ruc',
            'estado_civil',            
            'ocupacion',
            'profesion', 
            'ciudad',            
            "genero",
            'afinidad_laboral',
            'direccion',
            'estado',
            'token_firebase'                   
        )

    def get_estado(self, obj):
        """
        OBTIENE EL ESTADO DE LA RELACION INTERMEDIA
        SEGUN EL PK DE LA ORGANIZACION
        """
        try:
            organizacion = self.context["pk_voluntario"]
            return obj.get_estado(organizacion)
        except KeyError:
            return "No ha enviado pk de organizacion"        

class AceptarVoluntarioSerializer(serializers.ModelSerializer):
    """
    SERIALIZER SOBRE LOS VOLUNTARIOS DE LA ORGANIZACION
        ESTADOS = ACEPTADO __ NO_ACEPTADO
    """             
    class Meta:
        model = Voluntario_organizacion
        fields = (  
            'estado',       
        )

    def update(self, instance, validated_data):
        """
        METODO PARA ACEPTAR VOLUNTARIO
        """        
        instance.fecha_registro_voluntario = datetime.date.today()
        instance.estado = validated_data.get('estado')        
        instance.save()
        mensaje = instance.voluntario.nombre +" aceptado como voluntario de "+instance.organizacion.nombre
        Notificaciones.objects.create(organizacion=instance.organizacion.pk,voluntario=instance.voluntario.pk,descripcion=mensaje)
        return validated_data

    def validate(self, validated_data):        
        if(not validated_data.get('estado')):
            raise ValidationError(_("No ha ingresado valor de estado"))
        return validated_data

class SolicitudVoluntarioSerializer(serializers.ModelSerializer):
    """
    SERIALIZER SOBRE LOS VOLUNTARIOS DE LA ORGANIZACION
        ESTADOS = ACEPTADO __ NO_ACEPTADO
        PARA ENVIAR SOLICITUD DE VOLUNTARIO
    """             
    class Meta:
        model = Voluntario_organizacion
        fields = (  
            'organizacion',            
            'voluntario'
        )

    def create(self, validated_data):
        instance = Voluntario_organizacion.objects.create(**validated_data,estado='NO_ACEPTADO')        
        mensaje = instance.voluntario.nombre +" solicito ser voluntario de "+instance.organizacion.nombre
        Notificaciones.objects.create(organizacion=instance.organizacion.pk,voluntario=instance.voluntario.pk,descripcion=mensaje)
        return validated_data

class VoluntarioSerializer(serializers.ModelSerializer):
    """
    SERIALIZER SOBRE LOS VOLUNTARIOS DE LA ORGANIZACION
        ESTADOS = ACEPTADO __ NO_ACEPTADO
    """             
    usuario = UsuarioValidateSerializers()

    class Meta:
        model = Voluntario
        fields = (      
            'nombre', 
            'apellido',                       
            'genero',  
            'usuario',               
        )
    
    def create(self, validated_data):
        """
        METODO PARA CREAR VOLUNTARIO
            param: self
            param: validated_data 
        """        
        datos_usuario = validated_data.pop('usuario')  
        datos_usuario['password'] = make_password(datos_usuario.get('password'))
        usuario = Usuario.objects.create(**datos_usuario,tipo_usuario='VOLUNTARIO')        
        Voluntario.objects.create(**validated_data,estado_civil='',cedula_ruc='',ocupacion='',profesion='',
                                        afinidad_laboral='',direccion='',ciudad='',telefono='',descripcion='',
                                       usuario=usuario)
        AccountValidation.envioCorreoUsuario(self,datos_usuario.get('email'),validated_data.get('nombre'))
        return validated_data

    def validate(self, validated_data):
        return validated_data

class VoluntarioUpdateSerializer(serializers.ModelSerializer):
    """
    SERIALIZER SOBRE LOS VOLUNTARIOS DE LA ORGANIZACION
        ESTADOS = ACEPTADO __ NO_ACEPTADO
    """                 
    email = serializers.CharField(validators=[validar_email])

    class Meta:
        model = Voluntario
        fields = (      
            'nombre', 
            'apellido',
            'cedula_ruc',
            'estado_civil',            
            'ocupacion',
            'profesion', 
            'ciudad',            
            "genero",
            'afinidad_laboral',
            'direccion',            
            'email'      
        )

    def update(self, instance, validated_data):
        """
        METODO PARA ACEPTAR VOLUNTARIO
        """                
        instance.nombre = validated_data.get('nombre')
        instance.apellido = validated_data.get('apellido')
        instance.cedula_ruc = validated_data.get('cedula_ruc')
        instance.estado_civil = validated_data.get('estado_civil')
        instance.ocupacion = validated_data.get('ocupacion')
        instance.profesion = validated_data.get('profesion')
        instance.ciudad = validated_data.get('ciudad')
        instance.genero = validated_data.get('genero')
        instance.afinidad_laboral = validated_data.get('afinidad_laboral')
        instance.direccion = validated_data.get('direccion')

        instance.usuario.email = validated_data.get('email')
        instance.save()
        return validated_data      
        
    def validate(self, validated_data):
        return validated_data