from rest_framework.response import Response
from rest_framework import viewsets, status
from apps.usuarios.models import Voluntario, Organizacion, Notificaciones
from .serializers import NotificacionesSerializer
from rest_framework.pagination import PageNumberPagination
from django.http import QueryDict
from django.core.exceptions import ValidationError

class Pagination(PageNumberPagination):
    """
    Paginación para la Lista de personas
    """
    page_size = 20

    def get_paginated_response(self, data):
        return Response({
            'next': self.page.next_page_number() if self.page.has_next() else 0,
            'previous': self.page.previous_page_number() if self.page.has_previous() else 0,
            'count': self.page.paginator.count,
            'results': data
        })

class NotificacionesView(viewsets.ModelViewSet):
    """
    VISTA ACEPTACION DE VOLUNTARIO
    """        
    pagination_class = Pagination    
    serializer_class = NotificacionesSerializer
    queryset =  Notificaciones.objects.all()

    def retrieve(self, request, pk=None):
        try:
            queryset = Notificaciones.objects.filter(organizacion=pk) | Notificaciones.objects.filter(voluntario=pk)
            if "page" in request.query_params:
                queryset = self.paginate_queryset(queryset)
                serializer = NotificacionesSerializer(queryset,many=True)
                return self.get_paginated_response(serializer.data)
            serializer = NotificacionesSerializer(queryset,many=True)
            return Response(serializer.data,status=status.HTTP_200_OK) 
        except Notificaciones.DoesNotExist:
            return Response({"error": 'No existe voluntario con ese pk'}, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError:
            return Response({"error": 'Ha ingresado pk voluntario no valido'}, status=status.HTTP_400_BAD_REQUEST)