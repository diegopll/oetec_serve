from apps.usuarios.models import Notificaciones
from rest_framework import serializers

class NotificacionesSerializer(serializers.ModelSerializer):
    """
    SERIALIZER DE NOTIFICACIONES
    """               
    class Meta:
        model = Notificaciones
        fields = (  
            'descripcion',
        )