from rest_framework import serializers
from apps.usuarios.models import Red_social

class RedSocialSerializers(serializers.ModelSerializer):
    """
    Recursos del Usuario
    """
    class Meta:
        model = Red_social
        fields = (            
            'nombre',            
            'url',            
        )

    def validate(self, validated_data):
        """
        VALIDADORES DE DATOS
        """        
        return validated_data