import uuid
from django.db import models
from apps.utils.validadores.validadores import validar_cadenaString

class Categoria(models.Model):
    """ 
    MODELO CATEGORIA
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField(max_length=200,validators=[validar_cadenaString])
    descripcion = models.TextField(max_length=500, blank=True,null=True)    

    #DEFINE NOMBRE PARA APARECER EN ADMIN DJANGO
    def __str__(self):
        return "%s" % self.nombre