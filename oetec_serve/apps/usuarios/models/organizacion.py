import uuid
from django.db import models
from apps.utils.validadores.validadores import validar_cadenaString,verificar_cedula_ruc
from apps.usuarios import models as modelsGestion

class Organizacion(models.Model):
    """ 
    MODELO ORAGANIZACION
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField(max_length=200,validators=[validar_cadenaString])
    url = models.URLField(blank=True,null=True)
    cedula_ruc = models.CharField(max_length=13,validators=[verificar_cedula_ruc],unique=True)
    representante = models.CharField(max_length=200,validators=[validar_cadenaString])
    telefono = models.CharField(max_length=12, blank=True,null=True)
    descripcion = models.TextField(max_length=500, blank=True,null=True)

    fecha_registro = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    #RELACION CON TABLA USUARIO DE MUCHOS A UNO
    usuario = models.OneToOneField(
        "Usuario",
        on_delete=models.CASCADE,
        related_name="organizacion",        
    )
    
    #DEFINE NOMBRE PARA APARECER EN ADMIN DJANGO
    def __str__(self):
        if self.usuario is not None:
            return "%s" % self.nombre +" con usuario %s" % self.usuario.email
        return ""


    @property
    def obtener_red_social(self):
        """
        Obtiene la foto de perfil de la persona
        """                
        redSocial = self.red_social.all()
        listaJson = []
        if redSocial:
            lista = {}
            for x in redSocial:            
                lista[x.nombre.lower()] =  x.url        
            listaJson.append(lista)
        return listaJson

    def get_estado(self,voluntario):
        """
        Obtiene la puntuación del centro
        deportivo
        """        
        try:
            relacion = modelsGestion.Voluntario_organizacion.objects.get(voluntario=voluntario,organizacion=self.pk)
            print(relacion)
            return relacion.estado
        except modelsGestion.Voluntario_organizacion.DoesNotExist:
            return 'SIN_SOLICITUD'
