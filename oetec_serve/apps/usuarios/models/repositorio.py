import uuid
from django.db import models
from apps.utils.validadores.validadores import validar_fecha_publicacion,validar_cadenaString

class Repositorio(models.Model):
    """ 
    MODELO REPOSITORIO
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    titulo = models.CharField(max_length=200, blank=False,validators=[validar_cadenaString])
    url = models.URLField(max_length=200, blank=True)        
    descripcion = models.TextField(max_length=500, blank=True,null=True)
    fecha_acceso = models.DateTimeField(blank=True,null=True)
    fecha_publicacion = models.DateField(validators=[validar_fecha_publicacion])

    #RELACION CON TABLA USUARIO DE MUCHOS A UNO
    organizacion = models.ForeignKey(
        "Organizacion",
        on_delete=models.CASCADE,    
        related_name="repositorio",
    )

    #RELACION CON TABLA USUARIO DE MUCHOS A UNO
    categoria = models.ForeignKey(
        "Categoria",
        on_delete=models.CASCADE,    
        related_name="repositorio",
    )
    
    #DEFINE NOMBRE PARA APARECER EN ADMIN DJANGO
    def __str__(self):
        return "REPO: %s" % self.titulo +" de %s" %self.organizacion.nombre