import uuid
from django.db import models
from apps.utils.validadores.validadores import validar_cadenaString

class Red_social(models.Model):
    """ 
    MODELO RED SOCIAL
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField(max_length=200, blank=False,validators=[validar_cadenaString])
    url = models.URLField(max_length=200, null=True,blank=True)        

    #RELACION CON TABLA USUARIO DE MUCHOS A UNO
    organizacion = models.ForeignKey(
        "Organizacion",
        on_delete=models.CASCADE,    
        related_name="red_social",
    )

    #DEFINE NOMBRE PARA APARECER EN ADMIN DJANGO
    def __str__(self):
        return "%s" % self.nombre + " de %s" % self.organizacion.nombre