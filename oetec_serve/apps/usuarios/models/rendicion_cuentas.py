import uuid
from django.db import models
from apps.utils.validadores.validadores import validar_cadenaString

class Rendicion_cuentas(models.Model):
    """ 
    MODELO RENDICION DE CUENTAS
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    titulo = models.CharField(max_length=200, blank=False,validators=[validar_cadenaString])
    url = models.URLField(max_length=200, blank=True)        
    descripcion = models.TextField(max_length=500, blank=True,null=True)

    #RELACION CON TABLA USUARIO DE MUCHOS A UNO
    organizacion = models.ForeignKey(
        "Organizacion",
        on_delete=models.CASCADE,    
        related_name="rendicion_cuentas",
    )
    
    #DEFINE NOMBRE PARA APARECER EN ADMIN DJANGO
    def __str__(self):
        return "RD: %s" % self.titulo +" de %s" %self.organizacion.nombre