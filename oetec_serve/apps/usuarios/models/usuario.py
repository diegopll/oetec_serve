
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from apps.usuarios.usuario.manager import UserManager
from django.core.validators import ValidationError, validate_email
from django.utils.translation import gettext_lazy as _
from apps.usuarios import models as modelsGestion
import uuid
from apps.utils.validadores.validadores import validar_email,validar_passwword

ORGANIZACION = "ORGANIZACION"
VOLUNTARIO = "VOLUNTARIO"

TIPO_USUARIO = (
    (ORGANIZACION, "ORGANIZACION"),
    (VOLUNTARIO, "VOLUNTARIO"),
)

class Usuario(AbstractBaseUser, PermissionsMixin):
    """
    Modificación del Modelo Usuarios por defecto
    de django
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(max_length=255, unique=True,validators=[validar_email])
    password = models.CharField(max_length=100, editable=True,validators=[validar_passwword])
    token_firebase = models.CharField(max_length=255, editable=False, blank=True, null=True)
    tipo_usuario = models.CharField(choices=TIPO_USUARIO,
        max_length=20, default=VOLUNTARIO)
    objects = UserManager()  # Usar el User Manager
    fecha_registro = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)    
    
    USERNAME_FIELD = 'email'    
    
    def __str__(self):        
        return "%s" % self.email        

    def validate_isActive(self):
        if not self.is_active:
            raise ValidationError(_("Su cuenta aun no ha sido activada"))
        return True    