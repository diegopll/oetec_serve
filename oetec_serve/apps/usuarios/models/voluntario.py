import uuid
from django.db import models
from apps.utils.validadores.validadores import validar_cedula,validar_cadenaString

class Voluntario(models.Model):
    """ 
    MODELO VOLUNTARIO
    """

    MASCULINO = 'masculino'
    FEMENINO = 'femenino'

    GENERO = (
        (MASCULINO, 'MASCULINO'),
        (FEMENINO, 'FEMENINO')
    )

    SOLTERO = 'SOLTERO'
    CASADO = 'CASADO'
    DIVORCIADO = 'DIVORCIADO'

    ESTADO_CIVIL = (
        (SOLTERO, 'SOLTERO'),
        (CASADO, 'CASADO'),
        (DIVORCIADO,'DIVORCIADO')
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField(max_length=200)
    apellido = models.CharField(max_length=200)
    estado_civil = models.CharField(max_length=200,choices=ESTADO_CIVIL, default=SOLTERO,null=True,blank=True)
    cedula_ruc = models.CharField(max_length=10,validators=[validar_cedula])
    ocupacion = models.CharField(max_length=200,validators=[validar_cadenaString])
    profesion = models.CharField(max_length=200,validators=[validar_cadenaString])
    afinidad_laboral = models.CharField(max_length=200, null=True,blank=True,validators=[validar_cadenaString])
    direccion = models.CharField(max_length=200, null=True,blank=True)
    ciudad = models.CharField(max_length=200, null=True,blank=True,validators=[validar_cadenaString])
    telefono = models.CharField(max_length=12, null=True,blank=True)
    genero = models.CharField(max_length=30, choices=GENERO, default=MASCULINO)
    descripcion = models.TextField(max_length=500, null=True,blank=True)

    organizacion = models.ManyToManyField("Organizacion", through='Voluntario_organizacion')

    fecha_registro = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    #RELACION CON TABLA USUARIO DE MUCHOS A UNO
    usuario = models.OneToOneField(
        "Usuario",
        on_delete=models.CASCADE,
        related_name="voluntario",        
    )
    
    #DEFINE NOMBRE PARA APARECER EN ADMIN DJANGO
    def __str__(self):
        if self.usuario is not None:
            return "%s" % self.nombre +" con usuario %s" % self.usuario.email
        return "ASIGNE UN USUARIO"
    
    def get_estado(self,organizacion):
        """
        Obtiene la puntuación del centro
        deportivo
        """        
        relacion = self.voluntario_organizacion.get(organizacion=organizacion)
        return relacion.estado