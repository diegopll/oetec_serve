import uuid
from django.db import models

class Notificaciones(models.Model):
    """ 
    MODELO NOTIFICACIONES
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    organizacion = models.UUIDField(default=uuid.uuid4, editable=False)
    voluntario = models.UUIDField(default=uuid.uuid4, editable=False)
    descripcion = models.CharField(max_length=200)

    def __str__(self):
        return "%s" % self.descripcion