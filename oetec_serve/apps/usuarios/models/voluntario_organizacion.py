import uuid
from django.db import models

class Voluntario_organizacion(models.Model):
    """ 
    MODELO USUARIO_ORGANIZACION
    """

    ACEPTADO = 'ACEPTADO'
    NO_ACEPTADO = 'NO_ACEPTADO'

    ESTADOS = (
        (ACEPTADO, 'ACEPTADO'),
        (NO_ACEPTADO, 'NO_ACEPTADO')
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    estado = models.CharField(max_length=30, choices=ESTADOS, default=NO_ACEPTADO)

    organizacion = models.ForeignKey("Organizacion", on_delete=models.CASCADE,related_name="voluntario_organizacion",)
    voluntario = models.ForeignKey("Voluntario", on_delete=models.CASCADE,related_name="voluntario_organizacion",)

    fecha_registro_voluntario = models.DateField(null=True,blank=True)
    
    #DEFINE NOMBRE PARA APARECER EN ADMIN DJANGO
    def __str__(self):
        if self.voluntario is not None:
            return self.voluntario.nombre +" de "+self.organizacion.nombre
        return ""

    class Meta:
        unique_together = ('organizacion', 'voluntario')