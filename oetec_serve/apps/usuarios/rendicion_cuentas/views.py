from rest_framework.response import Response
from rest_framework import viewsets, status
from apps.usuarios.models import Rendicion_cuentas, Organizacion
from .serializers import OrganizacionListRendicionCuentaSerializer,RendicionCuentaSerializer
from django.shortcuts import get_object_or_404
from rest_framework.pagination import PageNumberPagination
from django.core.exceptions import ValidationError

class Pagination(PageNumberPagination):
    """
    Paginación para la Lista de personas
    """
    page_size = 20

    def get_paginated_response(self, data):
        return Response({
            'next': self.page.next_page_number() if self.page.has_next() else 0,
            'previous': self.page.previous_page_number() if self.page.has_previous() else 0,
            'count': self.page.paginator.count,
            'results': data
        })

class RendicionCuentaPersonalizadoView(viewsets.ReadOnlyModelViewSet):
    """
    REPOSITORIOS DE ORGANIAZION SEGUN PK
    """    
    pagination_class = Pagination    
    serializer_class = OrganizacionListRendicionCuentaSerializer
    queryset =  Rendicion_cuentas.objects.all().order_by('titulo')

    def retrieve(self, request, pk=None):
        """
        OBTIENE TODOS LOS REPOSITORIOS DE LA ORGANIZACION SEGUN SU PK O ID
            param: self
            param: request
            param: pk de organizacion
        """
        try:
            organizacion = get_object_or_404(Organizacion.objects.all(), pk=pk)
            queryset = Rendicion_cuentas.objects.filter(organizacion=organizacion).order_by('titulo')
            if "page" in request.query_params:
                queryset = self.paginate_queryset(queryset)
                serializer = OrganizacionListRendicionCuentaSerializer(queryset, many=True, context={"request": request})
                return self.get_paginated_response(serializer.data)
            serializer = OrganizacionListRendicionCuentaSerializer(queryset, many=True, context={"request": request})
            return Response(serializer.data,status=status.HTTP_200_OK)   
        except Organizacion.DoesNotExist:
            return Response({"error": 'No existe Organizacion con ese pk'}, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError:
            return Response({"error": 'Ha ingresado pk Organizacion no valido'}, status=status.HTTP_400_BAD_REQUEST)

class RendicionCuentasView(viewsets.ModelViewSet):
    """
    VIEW PARA CREAR Y EDITAR REPOSITORIO
    """    
    pagination_class = Pagination    
    serializer_class = RendicionCuentaSerializer    
    queryset =  Rendicion_cuentas.objects.all().order_by('titulo')        

    def create(self, request):
        serializer = RendicionCuentaSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):            
            serializer.save()                        
            return Response({"response": 'Guardado Correctamente'},status=status.HTTP_201_CREATED)
        return Response(serializer.erros,status=status.HTTP_400_BAD_REQUEST) 

    def update(self, request, pk=None):
        """        
            :param self: 
            :param request: 
            :param pk=None: 
        """            
        try:
            queryset = get_object_or_404(Rendicion_cuentas.objects.all(), pk=pk)
            serializer = RendicionCuentaSerializer(queryset,data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()            
                return Response({"response": 'Se ha actuailzado correctamente'},status=status.HTTP_201_CREATED)
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)    
        except Rendicion_cuentas.DoesNotExist:
            return Response({"error": 'No existe repositorio con ese pk'}, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError:
            return Response({"error": 'Ha ingresado pk repositorio no valido'}, status=status.HTTP_400_BAD_REQUEST)
        

    def retrieve(self, request, pk=None):
        try:
            queryset = Rendicion_cuentas.objects.all()
            user = get_object_or_404(queryset, pk=pk)
            serializer = RendicionCuentaSerializer(user)
            return Response(serializer.data,status=status.HTTP_200_OK)
        except Rendicion_cuentas.DoesNotExist:
            return Response({"error": 'No existe repositorio con ese pk'}, status=status.HTTP_400_BAD_REQUEST)
        except ValidationError:
            return Response({"error": 'Ha ingresado pk repositorio no valido'}, status=status.HTTP_400_BAD_REQUEST)
        