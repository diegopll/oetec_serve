from rest_framework import serializers
from apps.usuarios.models import Rendicion_cuentas

class OrganizacionListRendicionCuentaSerializer(serializers.ModelSerializer):
    """
    
    """        

    class Meta:
        model = Rendicion_cuentas
        fields = (   
            'pk',
            'titulo',
            'url',
            "descripcion",                     
        )

class RendicionCuentaSerializer(serializers.ModelSerializer):
    """
    
    """        
    class Meta:
        model = Rendicion_cuentas
        fields = (            
            'titulo',
            'url',
            "descripcion",
            'organizacion'                                     
        )

    def create(self, validated_data):
        Rendicion_cuentas.objects.create(**validated_data)
        return validated_data
    
    def update(self, instance, validated_data):        
        instance.titulo = validated_data.get('titulo')
        instance.url=validated_data.get('url')
        instance.descripcion =validated_data.get('descripcion')                 
        instance.organizacion=validated_data.get('organizacion')
        instance.save()
        return instance