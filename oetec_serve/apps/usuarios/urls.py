from django.conf.urls import url, include
from django.urls import path, re_path
from rest_framework_nested import routers
from rest_framework.routers import DefaultRouter
from apps.usuarios.organizacion.views import OrganizacionView
from apps.usuarios.repositorio.views import RepositorioPersonalizadoView, RepositorioView
from apps.usuarios.rendicion_cuentas.views import RendicionCuentaPersonalizadoView,RendicionCuentasView
from apps.usuarios.voluntario.views import VoluntarioOrganizacionView,AceptarVoluntarioView,VoluntarioView
from apps.usuarios.categoria.views import CategoriaListView
from apps.usuarios.notificaciones.views import NotificacionesView
from apps.usuarios.usuario.views import CambioClaveUsuarioView

app_name = 'api-app-usuarios'
router = DefaultRouter()
router.register(r'organizacion', OrganizacionView, base_name="organizacion")
router.register(r'repositorios-organizacion', RepositorioPersonalizadoView, base_name="repositorio-organizacion")
router.register(r'rendicion-cuenta-organizacion', RendicionCuentaPersonalizadoView, base_name="rendicion-cuenta-organizacion")
router.register(r'voluntario-organizacion', VoluntarioOrganizacionView, base_name="voluntario-organizacion")

router.register(r'repositorios', RepositorioView, base_name="repositorio")
router.register(r'categoria-list', CategoriaListView, base_name="categoria")
router.register(r'rendicion-cuenta', RendicionCuentasView, base_name="rendicion-cuenta")

router.register(r'solicitud-voluntario', AceptarVoluntarioView, base_name="solicitud-voluntario")

router.register(r'voluntario', VoluntarioView, base_name="voluntario")

router.register(r'notificaciones', NotificacionesView, base_name="notificaciones")

router.register(r'cambio-clave', CambioClaveUsuarioView, base_name="notificaciones")

urlpatterns = [
    path('', include(router.urls)),    
]
